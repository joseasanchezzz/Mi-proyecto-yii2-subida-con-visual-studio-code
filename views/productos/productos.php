<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div>
	<h1>TITULO</h1>
</div>


<?php $form = ActiveForm::begin(["method"=>"post",'enableClientValidation'=>true])  ?>



<div class="form-group">
	<?= $form->field($model,"codigo")->input("text") ?>
</div>
<div class="form-group">

	<?= $form->field($model,"nombre")->input("text")?>
</div>

<div class="form-group">
	<?= $form->field($model,"cantidad")->input("number") ?>
</div>

<div class="form-group">
	<?= $form->field($model,"precio")->input("number") ?>
</div>


	<?= Html::submitButton("Enviar" ,["class"=> "btn btn-primary"]) ?>



<?php $form->end();