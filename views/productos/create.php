<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$this->title = 'Registrar';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><a href="<?= Url::toRoute("productos/ver") ?>" >VER PRODUCTOS</a></h2>
<h1>Registrar Productos</h1>

<?php $form= ActiveForm::begin(["method"=>"post","enableClientValidation"=>true,]) ?>

<div class="form-group">

	<?= $form->field($model,"codigo")->input("text") ?>
	
</div>
<div class="form-group">

	<?= $form->field($model,"nombre")->input("text") ?>
	
</div>
<div class="form-group">

	<?= $form->field($model,"cantidad")->input("number") ?>
	
</div>
<div class="form-group">

	<?= $form->field($model,"precio")->input("number") ?>
	
</div>

<?= Html::submitButton("Enviar",["class"=> "btn btn-primary"])?>



<?php $form->end()?>
<h1><?= $mensaje ?></h1>